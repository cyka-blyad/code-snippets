import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Launcher
{
	private static final String delimiter = ",";

	public static void main(String[] args)
	{
		int[] array = userInput();
		Permutations perm = new Permutations(array);
		int[][] totalPermutations = perm.getPermutationsArray();
		for (int[] totalPermutation : totalPermutations) { printArray(totalPermutation); }
	}

	private static int[] userInput()
	{
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		Scanner input = new Scanner(System.in);
		System.out.print("Insert your desired numbers separated by \"" + delimiter + "\" :  ");
		String stringedArray = input.nextLine();
		StringTokenizer tokens = new StringTokenizer(stringedArray, delimiter);
		while (tokens.hasMoreTokens()) { arrayList.add(Integer.parseInt(tokens.nextToken())); }
		int[] finalArray = new int[arrayList.size()];
		for(int i = 0; i < arrayList.size(); i++) { finalArray[i] = arrayList.get(i); }
		return finalArray;
	}

	private static void printArray(int[] arr)
	{
		for( int val : arr ) { System.out.print(val + " "); }
		System.out.println("");
	}
}
