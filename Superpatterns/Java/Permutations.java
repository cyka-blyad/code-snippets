public class Permutations
{
	private int[] initialArray;
	private int[][] permutationsArray;
	private static int counter = 0;

	public Permutations(int[] initialArray)
	{
		this.initialArray = initialArray;
		this.permutationsArray = new int[factorialCalculate(this.initialArray.length)][this.initialArray.length];
	}

	private int factorialCalculate(int size)
	{
		int factorial = 1;

		for ( int i = size; i > 0; i-- )
		{
			factorial *= i;
		}

		return factorial;
	}

	private void swap(int var1, int var2, int[] arr)
	{
		int temp = arr[var1];
		arr[var1] = arr[var2];
		arr[var2] = temp;
	}

	private void arrayAdd(int[] array, int[][] permutationsArray)
	{
		if (counter < factorialCalculate(array.length))
		{
			for(int i = 0; i < array.length; i++)
			{
				(permutationsArray[counter])[i] = array[i];
			}
			counter++;
		}
	}

	private void genPermutations(int[] initialArray, int start, int end)
	{
		if (start == end)
		{
			arrayAdd(initialArray, permutationsArray);
			return;
		}

		int i;
		for (i = start; i <= end; i++)
		{
			swap(i, start, initialArray);
			genPermutations(initialArray, start+1, end);
			swap(i, start, initialArray);
		}
	}

	public int[][] getPermutationsArray()
	{
		genPermutations(initialArray, 0, (initialArray.length - 1));
		return permutationsArray;
	}
}
