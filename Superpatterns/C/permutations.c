#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CDELIMITER ','
#define SDELIMITER ","
#define MAX 255

int isNumber(char *number);
int* arrayInput(int *size);
int factorialCalculate(int number);
void printArray(int *array, int size);
void arrayAdd(int *array, int totalPermutations, int** permutationsArray);
void swap(int *val1, int *val2);
void permutations(int *array, int size, int start, int end, int **permutationsArray);
void permutationsDestroy(int **permutations, int totalPermutations);

int main()
{
	int size = 0,
		*array = arrayInput(&size),
		**permutationsArray = (int **)calloc(factorialCalculate(size), sizeof(array));
	permutations(array, size, 0, size - 1, permutationsArray);
	free(array);

	permutationsDestroy(permutationsArray, factorialCalculate(size));
	return 0;
}

int isNumber(char *number)
{
	int numeral = 1;
	for (int i = 0; i < strlen(number); i++)
	{
		if (isdigit(number[i]) == 0)
		{
			numeral = 0;
		}
	}
	return numeral;
}

int* arrayInput(int *size)
{
	char *stringedArray = (char *) calloc(MAX, sizeof(char)), i;
	int counter = 0;
	*size = 1;

	printf("Insert the desired numbers to create an permutation, separated by \"%c\" (Max %d): ",CDELIMITER,MAX);
	scanf("%s",stringedArray);
	for (int i = 0; stringedArray[i] != '\0'; i++)
	{
		if (stringedArray[i] == CDELIMITER)
		{
			(*size)++;
		}
	}

	int *permutation = (int *) calloc((*size), sizeof(int));
	char *token = strtok(stringedArray, SDELIMITER);
	while (token != NULL)
	{
		if (isNumber(token) == 1)
		{
			permutation[counter] = atoi(token);
			counter++;
		}
		token = strtok(NULL, SDELIMITER);
	}

	free(stringedArray);

	return permutation;
}

int factorialCalculate(int number)
{
	unsigned int n = 1;
	if (number > 0)
	{
		for (unsigned int i = number; i > 0; i--)
		{
			n *= i;
		}
	}
	return n;
}


void printArray(int *array, int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%d\t", array[i]);
	}
	printf("\n");
}

void arrayAdd(int *array, int size, int** permutationsArray)
{
	static int counter = 0;
	if (counter < factorialCalculate(size))
	{
		permutationsArray[counter] = (int *) calloc(size,sizeof(int));
		for (int i = 0; i < size; i++)
		{
			(permutationsArray[counter])[i] = array[i];
		}
		counter++;
	}
}

void swap(int *val1, int *val2)
{
	int temp = 0;
	temp = *val1;
	*val1 = *val2;
	*val2 = temp;
}

void permutations(int *array, int size, int start, int end, int **permutationsArray)
{
	if (start == end)
	{
		arrayAdd(array, size, permutationsArray);
		return;
	}

	int i;
	for (i = start; i <= end; i++)
	{
		swap((array+i), (array+start));
		permutations(array, size, start+1, end, permutationsArray);
		swap((array+i), (array+start));
	}
}

void permutationsDestroy(int **permutations, int totalPermutations)
{
	for(int i = 0; i < totalPermutations; i++)
	{
		free(permutations[i]);
	}
	free(permutations);
}
