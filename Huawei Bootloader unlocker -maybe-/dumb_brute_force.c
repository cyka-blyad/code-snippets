/*Based from Martazza's bootloader unlocker wich isn't really that
  quick since it has to run through 10^12 combinations and on my rig
  can only do around 100/sec (probably the fastest it gets bc of usb
  and adb limitations) which would take 33 years.
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Compile the program not only with gcc stock optimizations, but also with
 * gcc -o brute -flto -march=native -pipe -faggressive-loop-optimizations dumb_brute_force.c
 */
int main()
{
	/*
	 * Declairing Vars
	 *
	 * unsigned long is exactly 18446744073709551615
	 * unsigned long long is overkill
	 * also using global vars is bad for coding and optimization in general
	 *
	 * android bootloader unlock codes are always 16 digits.
	 */
	unsigned long baseStart = 0;
	char fou[] = "fastboot oem unlock ", command[strlen(fou) + 16 + 14];

	// Generating initString
	sprintf(command, "%s%016lu 2> /dev/null", fou, baseStart);

	for ( int i = 0, state = 1; (state != 0 && baseStart < 10000000000000000); i++ )
	{
		//print only 1000 perms at a time since printf is a time
		//demanding function
		if (i == 1000)
		{
			i = 0;
			printf("%016lu\n", baseStart);
		}
		sprintf(command, "%s%016lu 2> /dev/null", fou, baseStart++);
		state = system(command);
	}

	printf("\n\nYour unlock code is: %016lu\n\n", baseStart);
	return 0;
}